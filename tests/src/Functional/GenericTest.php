<?php

declare(strict_types=1);

namespace Drupal\Tests\s3fs_cors\Functional;

use Drupal\Tests\system\Functional\Module\GenericModuleTestBase;

/**
 * Generic module test for rest.
 *
 * @group s3fs_cors
 */
class GenericTest extends GenericModuleTestBase {}
