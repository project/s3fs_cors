<?php

declare(strict_types=1);

namespace Drupal\Tests\s3fs_cors\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Test s3fs_cors functionality.
 *
 * @group s3fs_cors
 */
final class S3fsCorsTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'file',
    'image',
    'node',
    's3fs_cors',
    's3fs',
  ];

  /**
   * Test module configuration.
   */
  public function testConfiguration(): void {
    $admin_user = $this->drupalCreateUser([
      'administer s3fs CORS',
    ]);
    $this->drupalLogin($admin_user);
    $this->drupalGet(Url::fromRoute('s3fs_cors.admin_form'));
    $this->assertSession()->elementExists('xpath', '//h1[text() = "CORS Upload"]');
    $this->submitForm([
      's3fs_https' => 'https',
      's3fs_access_type' => 'public-read',
    ], 'Save configuration');
    $this->assertSession()->pageTextContains('No values have been saved. Please check S3 File System settings first.');
  }

  /**
   * Test field configuration.
   */
  public function testField(): void {
    $admin_user = $this->drupalCreateUser([
      'access administration pages',
      'access content overview',
      'administer nodes',
      'bypass node access',
    ]);
    $this->drupalLogin($admin_user);

    // Create a test content type.
    $this->drupalCreateContentType(['type' => 's3fs_cors']);

    $file_field_storage = FieldStorageConfig::create([
      'field_name' => 'field_s3file',
      'entity_type' => 'node',
      'type' => 's3fs_cors_file',
    ]);
    $file_field_storage->save();
    $file_field = FieldConfig::create([
      'field_name' => $file_field_storage->getName(),
      'field_storage' => $file_field_storage,
      'entity_type' => 'node',
      'bundle' => 's3fs_cors',
      'label' => $this->randomString(),
    ]);
    $file_field->save();

    $image_field_storage = FieldStorageConfig::create([
      'field_name' => 'field_s3image',
      'entity_type' => 'node',
      'type' => 'image',
    ]);
    $image_field_storage->save();
    $image_field = FieldConfig::create([
      'field_name' => $image_field_storage->getName(),
      'field_storage' => $image_field_storage,
      'entity_type' => 'node',
      'bundle' => 's3fs_cors',
      'label' => $this->randomString(),
    ]);
    $image_field->save();

    // Assign widget settings for the default form mode.
    $settings = [
      'region' => 'content',
      'settings' => [
        'max_filesize' => '1GB',
      ],
      'third_party_settings' => [],
    ];

    /** @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $display_repository */
    $display_repository = \Drupal::service('entity_display.repository');
    $display_repository->getFormDisplay('node', 's3fs_cors')
      ->setComponent($file_field_storage->getName(), ['type' => 's3fs_cors_file_widget'] + $settings)
      ->setComponent($image_field_storage->getName(), ['type' => 's3fs_cors_image_widget'] + $settings)
      ->save();
    $display_repository->getViewDisplay('node', 's3fs_cors')
      ->setComponent($file_field_storage->getName(), ['type' => 's3fs_cors_file_default'])
      ->save();

    $this->drupalGet('/node/add/s3fs_cors');
    $assert = $this->assertSession();
    // Assert an expected timezone is available to select.
    $assert->pageTextContains('Bucket cannot be empty');
  }

}
