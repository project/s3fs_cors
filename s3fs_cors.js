/**
 * @file
 * Provides JavaScript additions to the managed file field type.
 *
 * This file provides progress bar support (if available), popup windows for
 * file previews, and disabling of other file fields during Ajax uploads (which
 * prevents separate file fields from accidentally uploading files).
 */

(function ($, Drupal, once) {
  /**
   * Attach behaviors to file element auto upload.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches triggers for the upload button.
   * @prop {Drupal~behaviorDetach} detach
   *   Detaches auto file upload trigger.
   */
  Drupal.behaviors.s3fsCorsAutoUpload = {
    attach(context, settings) {
      // Detach default Drupal file auto upload behavior from any s3fs cors file input elements.
      $(
        once.remove(
          'auto-file-upload',
          '.s3fs-cors-file input[type="file"]',
          context,
        ),
      ).off('.autoFileUpload');
      $(once.remove('auto-file-upload', 'input.s3fs-cors-upload', context)).off(
        '.autoFileUpload',
      );
      // Attach the custom s3fs cors auto upload processing behavior.
      $(once('s3fs-cors-auto-upload', '.s3fs-cors-file input[type="file"]')).on(
        'change.s3fsCorsAutoUpload',
        { settings: settings.s3fs_cors, baseUrl: settings.path.baseUrl },
        Drupal.s3fsCors.triggerUploadButton,
      );
      $(once('s3fs-cors-auto-upload', 'input.s3fs-cors-upload')).on(
        'change.s3fsCorsAutoUpload',
        { settings: settings.s3fs_cors, baseUrl: settings.path.baseUrl },
        Drupal.s3fsCors.triggerUploadButton,
      );
    },
    detach(context, settings, trigger) {
      if (trigger === 'unload') {
        $(
          once.remove(
            's3fs-cors-auto-upload',
            '.s3fs-cors-file input[type="file"]',
            context,
          ),
        ).off('.s3fsCorsAutoUpload');
      }
    },
  };

  /**
   * Attach behaviors to links within managed file elements for preview windows.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attaches triggers.
   * @prop {Drupal~behaviorDetach} detach
   *   Detaches triggers.
   */
  Drupal.behaviors.s3fsCorsPreviewLinks = {
    attach(context) {
      $(context)
        .find('div.js-form-managed-file .file a')
        .on('click', Drupal.s3fsCors.openInNewWindow);
    },
    detach(context) {
      $(context)
        .find('div.js-form-managed-file .file a')
        .off('click', Drupal.s3fsCors.openInNewWindow);
    },
  };

  /**
   * File upload utility functions.
   *
   * @namespace
   */
  Drupal.s3fsCors = Drupal.s3fsCors || {
    validators: {
      /**
       * Validates if the file size exceeds the maximum allowed size.
       *
       * @param {File} file
       *   The file to be validated.
       * @param {number} maxSize
       *   The maximum size allowed (in bytes).
       *
       * @returns {Promise<void>}
       *   Resolves if the file size is within the limit, otherwise rejects with an error message.
       */
      max_size(file, maxSize) {
        return file.size > maxSize
          ? Promise.reject(
              Drupal.t('Max size allowed is: %maxSizeMB', {
                '%maxSize': maxSize / 1048576,
              }),
            )
          : Promise.resolve();
      },

      /**
       * Validates if the file extension is within the allowed list.
       *
       * @param {File} file
       *   The file to be validated.
       * @param {string} extensionList
       *   Comma-separated list of allowed extensions.
       *
       * @returns {Promise<void>}
       *   Resolves if the file extension is allowed, otherwise rejects with an error message.
       */
      extension_list(file, extensionList) {
        const extensionPattern = extensionList.replace(/,\s*/g, '|');
        if (extensionPattern.length > 1 && file.name.length > 0) {
          const acceptableMatch = new RegExp(`\\.(${extensionPattern})$`, 'gi');
          if (!acceptableMatch.test(file.name)) {
            const error = Drupal.t(
              'The selected file %filename cannot be uploaded. Only files with the following extensions are allowed: %extensions.',
              {
                '%filename': file.name.replace('C:\\fakepath\\', ''),
                '%extensions': extensionPattern.replace(/\|/g, ', '),
              },
            );
            return Promise.reject(error);
          }
        }
        return Promise.resolve();
      },
    },

    /**
     * Validates a file using specified settings.
     *
     * @name Drupal.s3fsCors.validateFile
     *
     * @param {File} file
     *   The file to be validated.
     * @param {Object} settings
     *   Settings containing validators.
     *
     * @returns {Promise<void[]>}
     *   Resolves if all validations pass, otherwise rejects with an error.
     */
    validateFile(file, settings) {
      return Promise.all(
        Object.keys(settings.validators).map((validator) => {
          if (
            settings.validators[validator] &&
            typeof Drupal.s3fsCors.validators[validator] === 'function'
          ) {
            return Drupal.s3fsCors.validators[validator](
              file,
              settings.validators[validator],
            );
          }

          console.warn(
            Drupal.t('The %validator is missing.', { '%validator': validator }),
          );
          return Promise.resolve();
        }),
      );
    },

    /**
     * Triggers the upload button and handles file upload events.
     *
     * @name Drupal.s3fsCors.triggerUploadButton
     *
     * @param {Event} event
     *   The event triggered by the upload button.
     */
    triggerUploadButton(event) {
      /** @type HTMLInputElement */
      const fileInput = event.target;
      const form = fileInput.closest('form');
      form
        .querySelectorAll('input[type="submit"]')
        .forEach((input) => input.setAttribute('disabled', 'disabled'));

      if (!fileInput.files || !window.FormData) {
        alert(Drupal.t('CORS Upload is not supported in your browser. Sorry.'));
        return;
      }

      const baseUrl = event.data.baseUrl;
      const targetId = event.target.id.split('--')[0];
      const fieldName = targetId.split('-');
      const fieldNameArray = fieldName.slice(1, fieldName.length - 2);
      const delta = fieldName[fieldName.length - 2];
      const fieldNameKey = fieldNameArray.join('_');

      const settings = event.data.settings[fieldNameKey];
      const directory = settings.upload_location
        .split('::')[1]
        .replace(/\u002F/g, '::');
      const progressBars = [];

      Promise.all(
        [...fileInput.files].map((file, i) => {
          const progressBar = Drupal.s3fsCors.createProgressBar(fileInput);
          progressBars.push(progressBar);

          return Drupal.s3fsCors
            .validateFile(file, settings)
            .then(() =>
              Drupal.s3fsCors.fetchFileKey(baseUrl, directory, file, i),
            )
            .then((fileKeyData) =>
              Drupal.s3fsCors.uploadToS3(
                fileKeyData,
                file,
                settings,
                progressBar,
              ),
            )
            .then((fileKeyData) =>
              Drupal.s3fsCors.saveFileInDrupal(
                baseUrl,
                fileKeyData,
                fileInput,
                fieldNameKey,
                delta,
              ),
            );
        }),
      )
        .then((fids) =>
          Drupal.s3fsCors.updateFormWithFid(
            fids,
            targetId,
            fileInput,
            form,
            `${fieldNameKey}_${delta}`,
            settings,
          ),
        )
        .then((response) =>
          Drupal.s3fsCors.triggerDrupalBehaviors(
            response,
            fieldNameKey,
            settings,
          ),
        )
        .catch((error) => Drupal.s3fsCors.showError(error, fileInput))
        // Re-enable all the submit buttons and remove progress bars in the form.
        .finally(() => {
          progressBars.forEach((item) => item.remove());
          form
            .querySelectorAll('input[type="submit"]')
            .forEach((input) => input.removeAttribute('disabled'));
        });
    },

    /**
     * Fetches a unique file key for S3 upload.
     *
     * @name Drupal.s3fsCors.fetchFileKey
     *
     * @param {string} baseUrl
     *   The base URL for the request.
     * @param {string} directory
     *   The directory where the file will be uploaded.
     * @param {File} fileObj
     *   The file object to be uploaded.
     * @param {number} fileIdx
     *   The index of the file in the file list.
     *
     * @returns {Promise<Object>}
     *   Resolves with the file key data.
     */
    fetchFileKey(baseUrl, directory, fileObj, fileIdx) {
      return fetch(
        `${baseUrl}ajax/s3fs_cors_key/${encodeURIComponent(directory)}/${encodeURIComponent(fileObj.name)}/${fileObj.size}/${fileIdx}`,
      )
        .then((response) => {
          if (!response.ok) {
            throw new Error(Drupal.t('Failed to fetch file key'));
          }
          return response.json();
        })
        .then((fileKeyData) => {
          if (!fileKeyData.file_key) {
            throw new Error(Drupal.t('File with the name already exists'));
          }
          return fileKeyData;
        });
    },

    /**
     * Uploads a file to S3.
     *
     * @name Drupal.s3fsCors.uploadToS3
     *
     * @param {Object} fileKeyData
     *   Data for the file key.
     * @param {File} fileObj
     *   The file object to be uploaded.
     * @param {Object} settings
     *   Settings for the upload.
     * @param {HTMLElement} progressBar
     *   The progress bar element to update.
     *
     * @returns {Promise<Object>}
     *   Resolves when the upload is successful.
     */
    uploadToS3(fileKeyData, fileObj, settings, progressBar) {
      const formData = new FormData();
      formData.append('key', fileKeyData.file_key);
      formData.append('Content-Type', fileObj.type);
      for (const key in settings.cors_form_data) {
        formData.append(key, settings.cors_form_data[key]);
      }
      formData.append('file', fileObj);

      return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        xhr.open('POST', settings.cors_form_action, true);
        xhr.withCredentials = true;

        xhr.upload.onprogress = (event) => {
          if (event.lengthComputable) {
            const percent = Math.floor((event.loaded / event.total) * 100);
            if (percent !== 100) {
              progressBar.innerHTML = Drupal.t('Uploading: @percent%', {
                '@percent': percent,
              });
            } else {
              progressBar.innerHTML = Drupal.t('Saving File...');
            }
          }
        };

        xhr.onload = () => {
          if (xhr.status >= 200 && xhr.status < 300) {
            resolve(fileKeyData);
          } else {
            reject(new Error(Drupal.t('Failed to upload to S3')));
          }
        };

        xhr.onerror = () =>
          reject(new Error(Drupal.t('Failed to upload to S3')));
        xhr.send(formData);
      });
    },

    /**
     * Saves the uploaded file in Drupal.
     *
     * @name Drupal.s3fsCors.saveFileInDrupal
     *
     * @param {string} baseUrl
     *   The base URL for the request.
     * @param {Object} fileKeyData
     *   Data for the file key.
     * @param {HTMLInputElement} fileInput
     *   The file input element.
     * @param {string} fieldNameKey
     *   The field name key.
     * @param {string} delta
     *   The delta value.
     *
     * @returns {Promise<number>}
     *   Resolves with the file ID (fid).
     */
    saveFileInDrupal(baseUrl, fileKeyData, fileInput, fieldNameKey, delta) {
      return fetch(
        `${baseUrl}ajax/s3fs_cors_save/${encodeURIComponent(fileKeyData.file_key.replace(/\u002F/g, '::'))}/${encodeURIComponent(fileKeyData.file_name)}/${fileKeyData.file_size}/${fieldNameKey}_${delta}`,
      )
        .then((response) => {
          if (!response.ok) {
            throw new Error(Drupal.t('Failed to save file in Drupal'));
          }
          return response.json();
        })
        .then((data) => {
          if (!data.fid) {
            throw new Error(Drupal.t("File couldn't be saved"));
          }
          return data.fid;
        });
    },

    /**
     * Updates the form with the file ID (fid) after successful upload.
     *
     * @name Drupal.s3fsCors.updateFormWithFid
     *
     * @param {Array<number>} fids
     *   The file ID.
     * @param {string} targetId
     *   The target ID for the upload.
     * @param {HTMLInputElement} fileInput
     *   The file input element.
     * @param {HTMLFormElement} form
     *   The form element.
     * @param {string} fieldName
     *   The field name.
     * @param {Object} settings
     *   Settings for the upload.
     *
     * @returns {Promise<Object>}
     *   Resolves with the response data.
     */
    updateFormWithFid(fids, targetId, fileInput, form, fieldName, settings) {
      const fidSelector = targetId.replace(/upload$/, 'fids');
      const fidInput = document.querySelector(
        `[data-drupal-selector=${fidSelector}]`,
      );
      fidInput.value = fids.join(' ');
      const numberFileIds = fids.length;

      // Post the results to Drupal if all files have been processed.
      const formData = new FormData(form);
      const button = form.querySelector(`.js-form-submit[name^=${fieldName}]`);

      if (button) {
        formData.append('_triggering_element_name', button.name);
        formData.append('_triggering_element_value', button.value);
      }

      // Add some additional required fields into formData.
      formData.append('_drupal_ajax', 1);
      formData.append(
        'ajax_page_state[theme]',
        drupalSettings.ajaxPageState.theme,
      );
      formData.append(
        'ajax_page_state[theme_token]',
        drupalSettings.ajaxPageState.theme_token,
      );
      formData.append(
        'ajax_page_state[libraries]',
        drupalSettings.ajaxPageState.libraries,
      );

      return fetch(
        `?element_parents=${settings.element_parents}&ajax_form=1&_wrapper_format=drupal_ajax`,
        {
          method: 'POST',
          body: formData,
          credentials: 'include',
        },
      ).then((response) => {
        if (!response.ok) {
          throw new Error(Drupal.t('Cannot upload file...'));
        }
        return response.json();
      });
    },

    /**
     * Triggers Drupal behaviors after file upload.
     *
     * @name Drupal.s3fsCors.triggerDrupalBehaviors
     *
     * @param {Object} response
     *   The response from the AJAX request.
     * @param {string} fieldNameKey
     *   The field name key.
     * @param {Object} settings
     *   Settings for the upload.
     *
     * @returns {Promise<void>}
     *   Resolves when behaviors are triggered.
     */
    triggerDrupalBehaviors(response, fieldNameKey, settings) {
      // Set the relevant selector in any of the returned Ajax commands that have a null selector.
      const responseLength = response.length;
      for (let i = 0; i < responseLength; i++) {
        const selector = response[i].selector;
        if (selector === null) {
          // Find the first descendant div with an id beginning with "ajax-wrapper".
          let fieldAjaxWrapper = document.querySelector(
            `div[data-drupal-selector="edit-${fieldNameKey.replace(
              /_/g,
              '-',
            )}-wrapper"]`,
          );
          let childDivId = '';

          do {
            fieldAjaxWrapper =
              fieldAjaxWrapper.querySelector('div:first-child');
            childDivId = fieldAjaxWrapper.id;
          } while (
            childDivId === '' ||
            childDivId.indexOf('ajax-wrapper') === -1
          );

          response[i].selector = `#${childDivId}`;
        }
      }

      // Create a Drupal.Ajax object without associating an element, a progress indicator or a URL.
      const ajaxObject = Drupal.ajax({
        url: `?element_parents=${
          settings.element_parents
        }&ajax_form=1&_wrapper_format=drupal_ajax`,
        base: false,
        element: false,
        progress: false,
      });

      // Then, simulate an AJAX response having arrived, and let the Ajax system handle it.
      const promise = ajaxObject.success(response, 'success');
      Drupal.attachBehaviors();

      return promise;
    },

    /**
     * Creates a progress bar for the file upload.
     *
     * @name Drupal.s3fsCors.createProgressBar
     *
     * @param {HTMLInputElement} fileInput
     *   The file input element.
     *
     * @returns {HTMLElement}
     *   The progress bar element.
     */
    createProgressBar(fileInput) {
      const progressBar = document.createElement('div');
      progressBar.id = 's3fs-cors-progress-0';
      progressBar.className = 'progress-bar';
      progressBar.style =
        'width: 100%; line-height: 2em; min-height: 2em; float: left; text-align: left;';
      progressBar.textContent = Drupal.t('Preparing upload ...');
      fileInput.style.display = 'none';
      fileInput.insertAdjacentElement('beforebegin', progressBar);
      return progressBar;
    },

    /**
     * Displays an error message for file upload errors.
     *
     * @name Drupal.s3fsCors.showError
     *
     * @param {string} message
     *   The error message to display.
     * @param {HTMLInputElement} fileInput
     *   The file input element.
     */
    showError(message, fileInput) {
      const errorDiv = document.createElement('div');
      errorDiv.className = 'messages messages--error file-upload-js-error';
      errorDiv.setAttribute('aria-live', 'polite');
      errorDiv.textContent = message;
      fileInput
        .closest('div.js-form-managed-file')
        .insertAdjacentElement('afterbegin', errorDiv);
      fileInput.value = '';

      console.error(message);
    },

    /**
     * Opens a link in a new window.
     *
     * @name Drupal.s3fsCors.openInNewWindow
     *
     * @param {Event} event
     *   The event triggered by clicking the link.
     */
    openInNewWindow(event) {
      event.preventDefault();
      /** @type HTMLLinkElement link */
      const link = event.target;

      link.setAttribute('target', '_blank');
      window.open(
        link.href,
        'filePreview',
        'toolbar=0,scrollbars=1,location=1,statusbar=1,menubar=0,resizable=1,width=500,height=550',
      );
    },
  };
})(jQuery, Drupal, once);
