<?php

declare(strict_types=1);

namespace Drupal\s3fs_cors\Element;

use Drupal\file\Element\ManagedFile;

/**
 * Provides an S3Cors File Element.
 *
 * @FormElement("s3cors_file")
 *
 * @deprecated in drupal:10.2.0 and is removed from drupal:12.0.0. Switch to s3fs_cors_file.
 * This form element uses Annotations and will not be upgraded to
 * PHP Attributes. This effectively means that it will be removed before
 * Drupal 12.
 * @see https://www.drupal.org/project/s3fs_cors/issues/3434300
 */
class S3CorsFile extends ManagedFile {

  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    @trigger_error('This is deprecated in drupal:10.2.0 and is removed from drupal:12.0.0. Switch to s3fs_cors_file. See https://www.drupal.org/project/s3fs_cors/issues/3434300', E_USER_DEPRECATED);
  }

}
