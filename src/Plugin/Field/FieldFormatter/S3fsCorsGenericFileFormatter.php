<?php

declare(strict_types=1);

namespace Drupal\s3fs_cors\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\file\Plugin\Field\FieldFormatter\GenericFileFormatter;

/**
 * Plugin implementation of 's3fs_cors_file_default' for S3fs CORS files.
 */
#[FieldFormatter(
  id: 's3fs_cors_file_default',
  label: new TranslatableMarkup('Generic s3fs cors file'),
  field_types: [
    's3fs_cors_file',
  ],
)]
class S3fsCorsGenericFileFormatter extends GenericFileFormatter {}
