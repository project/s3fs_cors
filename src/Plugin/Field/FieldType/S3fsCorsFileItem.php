<?php

declare(strict_types=1);

namespace Drupal\s3fs_cors\Plugin\Field\FieldType;

use Drupal\Component\Utility\Bytes;
use Drupal\Core\Field\Attribute\FieldType;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\file\Plugin\Field\FieldType\FileFieldItemList;
use Drupal\file\Plugin\Field\FieldType\FileItem;

/**
 * Plugin implementation of the 's3fs_cors_file' field type.
 */
#[FieldType(
  id: "s3fs_cors_file",
  label: new TranslatableMarkup("S3fs CORS File"),
  description: [
    new TranslatableMarkup("For uploading files directly to S3."),
    new TranslatableMarkup("Can be configured with options such as allowed file extensions and maximum upload size"),
  ],
  category: "file_upload",
  default_widget: "s3fs_cors_file_widget",
  default_formatter: "s3fs_cors_file_default",
  list_class: FileFieldItemList::class,
  constraints: ["ReferenceAccess" => [], "FileValidation" => []],
  column_groups: [
    'target_id' => [
      'label' => new TranslatableMarkup('File'),
      'translatable' => TRUE,
    ],
    'display' => [
      'label' => new TranslatableMarkup('Display'),
      'translatable' => TRUE,
    ],
    'description' => [
      'label' => new TranslatableMarkup('Description'),
      'translatable' => TRUE,
    ],
  ],
)]
class S3fsCorsFileItem extends FileItem {

  /**
   * {@inheritdoc}
   */
  public function getUploadValidators(): array {
    $validators = parent::getUploadValidators();

    // Special size limit applies to S3 CORS files.
    // This is currently 5 GB until the AWS S3 multipart upload functionality
    // is implemented.
    if ($this->getSettings()['uri_scheme'] === 's3') {
      $validators['FileSizeLimit']['fileLimit'] = Bytes::toNumber('5 GB');
    }

    return $validators;
  }

}
