<?php

namespace Drupal\s3fs_cors\Plugin\Field\FieldType;

use Drupal\Core\TypedData\ComplexDataDefinitionInterface;
use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\file\Plugin\Field\FieldType\FileItem;

/**
 * Plugin implementation of the 's3cors_file' field type.
 *
 * @FieldType(
 *   id = "s3cors_file",
 *   label = @Translation("S3 Cors File [deprecated]"),
 *   description = @Translation("This field stores the ID of a file as an integer value."),
 *   category = "file_upload",
 *   default_widget = "s3fs_cors_file_widget",
 *   default_formatter = "s3fs_cors_file_default",
 *   list_class = "\Drupal\file\Plugin\Field\FieldType\FileFieldItemList",
 *   constraints = {"ReferenceAccess" = {}, "FileValidation" = {}}
 * )
 *
 * @deprecated in drupal:10.2.0 and is removed from drupal:12.0.0. Switch to s3fs_cors_file.
 * This field type uses Annotations and will not be upgraded to PHP Attributes.
 * This effectively means that it will be removed before Drupal 12.
 * @see https://www.drupal.org/project/s3fs_cors/issues/3434300
 */
class S3CorsFileItem extends FileItem {

  public function __construct(ComplexDataDefinitionInterface $definition, $name = NULL, ?TypedDataInterface $parent = NULL) {
    parent::__construct($definition, $name, $parent);
    @trigger_error('This is deprecated in drupal:10.2.0 and is removed from drupal:12.0.0. Switch to s3fs_cors_file. See https://www.drupal.org/project/s3fs_cors/issues/3434300', E_USER_DEPRECATED);
  }

}
